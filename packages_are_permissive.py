import subprocess
import sys
import csv
import json

def get_packages_and_licenses():
    license_result = subprocess.run(['pip-licenses', '--format=json'], capture_output=True, text=True)
    if license_result.returncode != 0:
        print("Error running pip-licenses")
        return
    output = license_result.stdout
    package_data = json.loads(output)

    packages = {}
    for entry in package_data:
        packages[entry["Name"]] = entry["License"]
    return packages

def package_is_empty(package):
    if not package:
        return True
    return False

def package_on_whitelist(package):
   if package in whitelist:
       return True
   else:
       return False

def license_is_permissive(license):
    if license in permissive_licenses:
        return True
    return False

def package_is_permissive(licenses):
    license_list = licenses.split('; ')
    for license in license_list:
        if not license_is_permissive(license):
            return False
    else:
        return True

WHITELIST_PATH = 'whitelist.csv'
PERMISSIVE_LICENSES_PATH = 'permissive_licenses.csv'

with open(WHITELIST_PATH, 'r') as f:
    whitelist = [row[0] for row in csv.reader(f)]
with open(PERMISSIVE_LICENSES_PATH, 'r') as f:
    permissive_licenses = [row[0] for row in csv.reader(f)]

error_list = []
working_packages = get_packages_and_licenses()

for package,licenses in working_packages.items():
    if package_is_empty(package):
        error_list.append(f"{package}: License info empty.")
        continue
    if package_on_whitelist(package):
        continue
    if package_is_permissive(licenses):
        continue
    else:
        error_list.append(f"{package}:Licenses: <{licenses}>")

if error_list:
    print("\n".join(error_list))
    sys.exit(1)
print("pre-commit hook success")
sys.exit(0)
